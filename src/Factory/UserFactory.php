<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\User;
use App\Model\UserModel;

class UserFactory
{
    public function createFromModel(UserModel $userModel): User
    {
        return new User($userModel->username, $userModel->password, $userModel->firstName, $userModel->lastName);
    }
}
