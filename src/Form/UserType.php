<?php

declare(strict_types=1);

namespace App\Form;

use App\Model\UserModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ;

        if (in_array('create', $options['validation_groups'])) {
            $builder
                ->add('username')
                ->add('password')
                ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => UserModel::class,
                'validation_groups' => ['Default'],
                'csrf_protection' => false,
            ]);
    }
}