<?php

declare(strict_types=1);

namespace App;

use App\Entity\User;

class UserView
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getId(): string
    {
        return $this->user->getId();
    }

    public function getUsername(): string
    {
        return $this->user->getUsername();
    }

    public function getName(): string
    {
        return $this->user->getFirstName() . " " . $this->user->getLastName();
    }
}