<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Traits\FormErrorsTrait;
use App\Entity\User;
use App\Factory\UserFactory;
use App\Form\UserType;
use App\Model\UserModel;
use App\Repository\UserRepository;
use App\UserView;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    use FormErrorsTrait;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var UserFactory
     */
    private $userFactory;

    public function __construct(UserRepository $repository, FormFactoryInterface $formFactory, SerializerInterface $serializer, UserFactory $userFactory)
    {
        $this->repository = $repository;
        $this->formFactory = $formFactory;
        $this->serializer = $serializer;
        $this->userFactory = $userFactory;
    }

    /**
     * @Route("/users", name = "get_users", methods={"GET"}, requirements = {"_format" = "json"})
     */
    public function getUsersAction(): JsonResponse
    {
        $users = $this->repository->findAll();

        return $this->json(array_map(function (User $user): UserView {
            return new UserView($user);
        }, $users));
    }

    /**
     * @Route("/users", name = "post_users", methods = {"POST"}, requirements = {"_format" = "json"})
     */
    public function postUsersAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(UserType::class, new UserModel(), [
            'validation_groups' => ['Default', 'create'],
        ]);

        $data = \json_decode($request->getContent(), true);

        $form->submit($data);

        if (!$form->isValid()) {
            return $this->json($this->extractErrors($form), Response::HTTP_BAD_REQUEST);
        }

        $user = $this->userFactory->createFromModel($form->getData());

        $this->repository->save($user);

        return $this->json(new UserView($user));
    }

    /**
     * @Route("/users/{id}", name = "put_users", methods = {"PUT"}, requirements = {"_format" = "json"})
     */
    public function putUsersAction(Request $request, string $id): JsonResponse
    {
        $user = $this->repository->findOneById($id);

        if (!$user) {
            throw $this->createNotFoundException("Nie znaleziono użytkownika o id: " . $id);
        }

        $form = $this->formFactory->create(UserType::class, new UserModel());

        $data = \json_decode($request->getContent(), true);

        $form->submit($data);

        if (!$form->isValid()) {
            return $this->json($this->extractErrors($form), Response::HTTP_BAD_REQUEST);
        }

        $userModel = $form->getData();
        $user->hydrate($userModel);

        $this->repository->save($user);

        return $this->json(new UserView($user));
    }
}