<?php

declare(strict_types=1);

namespace App\Controller\Traits;

use Symfony\Component\Form\FormInterface;

trait FormErrorsTrait
{
    private function extractErrors(FormInterface $form): array
    {
        $formErrors = $form->getErrors(true);
        $errors = [];

        foreach ($formErrors as $formError) {
            $errors[$formError->getCause()->getPropertyPath()] = $formError->getMessage();
        }

        return $errors;
    }
}
