<?php

declare(strict_types=1);

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserModel
{
    /**
     * @var string|null
     *
     * @Assert\NotBlank(groups = {"create"})
     * @Assert\Length(min = 3, max = 32)
     */
    public $username;

    /**
     * @var string|null
     *
     * @Assert\NotBlank
     * @Assert\Length(min = 3)
     */
    public $firstName;

    /**
     * @var string|null
     *
     * @Assert\NotBlank
     * @Assert\Length(min = 3)
     */
    public $lastName;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(groups = {"create"})
     * @Assert\Length(min = 3)
     */
    public $password;
}
